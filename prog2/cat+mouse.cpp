// g++ cat+mouse.cpp positions.cpp -o out
#include <iostream>
#include <cmath>
#include "positions.h"

using namespace std;
int MAXTIME = 30;
float PI = 3.14;

float reduceRadians(float radian) {
	// since a, a+2pi, a+4pi, .. all equal 
	return fmod(radian, 6.28318530717958647688);
}

float convertToRadian(float angle) {
	if (angle == 0.0) {
		return 360;
	}
	//return (reduceAngle(angle)*PI)/180;
	return reduceRadians(angle*PI/180);
}

// You define the GetPositions function.
// It should read legal cat and mouse positions from the user
// and return the position values in its two arguments.
void GetPositions (Position *cat, Position *mouse) {
	float catR, catA;
	cout << "Please enter cat's radius: ";
	cin >> catR;
	cout << "Please enter cat's angle: ";
	cin >> catA;
	catA = convertToRadian(catA);
	cat->SetAbsolutePosition(catR, catA);

	float mouseA;
	cout << "Please enter mouse' angle: ";
	cin >> mouseA;
	mouseA = convertToRadian(mouseA);
	mouse->SetAbsolutePosition(1, mouseA);
}

// You define the RunChase function.
// Given initialized cat and mouse positions,
// it should simulate the cat chasing the mouse, printing the 
// result of each movement of cat and mouse.  Either the cat will 
// catch the mouse, or 30 time units will go by and the cat will 
// give up.
void RunChase (Position *cat, Position *mouse) {
    for (int i = 1; i < MAXTIME; i++) {
    	cout << "Time: " << i << " minutes passed\n";
    
    	// if the cat see's mouse and is not at the status
    	if (cat->Sees(*mouse) and !cat->IsAtStatue()) {
    		cat->IncrementPosition(1, 0);
    	// if the cat is at the statue or it didnt see the mouse, the move the cat around
    	} else {
    		cat->IncrementPosition(0, 1.25);
    	}

    	mouse->IncrementPosition(0,1);
    	cout << "Mouse's position: ";
	    mouse->Print();
	    cout << "Cat's position: ";
	    cat->Print();
    	if (cat->IsAtStatue() and mouse->IsBetween(*cat, *cat)) {
			// mouse has been caught
			cout << "Mouse has been caught!!!\n";
			break; 
		}
    }
}

int main () {
    Position cat = Position();
    Position mouse = Position();
    GetPositions (&cat, &mouse);
    RunChase (&cat, &mouse);
    return 0;
}

// mouse is caught without any movement (they start at the same location)
// cat radius = 1, cat angle = 35, mouse angle = 396

//cat catches the mouse on the very last step
// cat radius = 29, cat angle = 40, mouse angle = 40

// cat gives up but would've caught the mouse if he took one more step
// cat radius = 30, cat angle = 40, mouse angle = 40