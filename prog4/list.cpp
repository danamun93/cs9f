// g++ -g -Wall list.cpp
#include <iostream>
#include <cassert>
using namespace std;

#include "list.h"

ListNode::ListNode (int k) {
	myValue = k;
	myNext = 0;
}

ListNode::ListNode (int k, ListNode* ptr) {
	myValue = k;
	myNext = ptr;
}

// Delete the node and all nodes accessible through it.
// Precondition: this != 0.
ListNode::~ListNode () {
	// this version is buggy
	cout << "Deleting node with value " << myValue << endl;
	if (this->myNext != 0) {
		delete this->myNext;
		this->myNext = 0;
	}
}
// Print the list.
void ListNode::Print () {
	ListNode* list = this;
	for (; list; list = list->Rest()) {
		cout << list->First() << " ";
	}
	cout << endl;
}

// Return the value stored in the node.
int ListNode::First () {
	return myValue;
}

// Return the list of the remaining elements.
ListNode* ListNode::Rest () {
	return myNext;
}

ListNode* FromInput (istream &is) {
	int k;
	ListNode* head = 0;
	while (is >> k) {
		head = new ListNode (k, head);
	}
	return head;
}
	
int main () {
	ListNode* list = FromInput (cin);
	list->Print ();
	if (list) {
		delete list;
	}
	return 0;
}