#include <iostream>
using namespace std;

int addingMachine() {
	int input = 0;
	int subtotal = 0;
	int total = 0;

    // bool to indicate whether the consecutive zeros were seen
	bool terminate = false; 
	while (true)
	{
		cin >> input; 
		// the case when you saw the 0 twice and you need to terminate
		if (input == 0 and terminate) 
		{
			cout << "Total " << total << endl;
			return 0;
		}
		// the case where you want to print the subtotal because you saw a zero
		else if (input == 0) 
		{
			cout << "Subtotal " << subtotal << endl;
			total += subtotal;
			terminate = true;
			subtotal = 0;
		}
		else
		{
			terminate = false;
			subtotal += input;
		}
	}
}

int main() {
	addingMachine();
	return 0; 
}

//notes on how to run: g++ -g -Wall prog1.cpp
// ./a.out