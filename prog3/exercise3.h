#include <vector>
#include <string>
#include <unordered_map>
using namespace std;


class Board {
public:
	Board ();
	void SetCell (int player, int row, int col, char c);
	void Print (int player);
private:
	int currPlayer;
	vector<vector<char> > board;
};