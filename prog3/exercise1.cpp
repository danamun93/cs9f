#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
using namespace std;

void InterpretCommands (istream&);
void InterpretUpdate (istream&);
void InterpretList (istream&);
void InterpretBatch (istream&);
void InterpretQuit (istream&);

void InterpretCommands (istream& cmds) {
  string line, cmdWord;
  //____________________________ ;  // other definitions go here
  while ( getline(cmds, line) ) {   // get a line
    istringstream lineIn (line.c_str());
    if ( !(lineIn >> cmdWord) ) {  // get the line's first word
      cout << cmdWord << endl;
      cerr << "No words" << endl; // there weren't any words
    } else if (cmdWord=="update") {
      InterpretUpdate (lineIn);
    } else if (cmdWord=="list") {
      InterpretList (lineIn);
    } else if (cmdWord=="batch") {
      InterpretBatch (lineIn);
    } else if (cmdWord=="quit") {
      InterpretQuit (lineIn);
    } else {
      cerr << "Unrecognizable command word." << endl;
    }
  }
}

void InterpretUpdate (istream& is) {
  string word;
  int update;
  if (is >> word >> update) {
    // string and integer values were successfully read
    cout << "word: " << word << " update: " << update << endl;
  } else {
    // string and integer values were not successfully read
    cerr << "Unrecognizable command word." << endl;
  }
}

void InterpretList (istream& is) {
  string word;
  if (is >> word) {
    if (word == "names") {
      cout << "names" << endl;
    } else if (word == "quantities") {
      cout << "quantities" << endl; 
    } else {
      cerr << "Unrecognizable command word." << endl;
    }
  } else {
    // string and integer values were not successfully read
    cerr << "Unrecognizable command word." << endl;
  }
}

void InterpretBatch (istream& is) {
  //do the following: read, recognize, and process commands from the specified file, 
  //then (assuming that a quit command has not been read) resume reading commands from cin
  string filename;
  if (is >> filename) {
    //open the file
    string line;
    ifstream myfile (filename);
    if (myfile.is_open()) {
      while ( getline (myfile,line) ) {
        istringstream lineIn (line.c_str());
        InterpretCommands(lineIn);
      }
      myfile.close();
    }
    else {
      cout << "Unable to open file"; 
    }
  } else {
    // string and integer values were not successfully read
    cerr << "Unrecognizable command word." << endl;
  }
}

void InterpretQuit (istream& is) {
  exit(0);
}

int main () {
  InterpretCommands (cin);
  return 0;
}



