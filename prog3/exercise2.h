#include <vector>
#include <string>
#include <unordered_map>
using namespace std;


class Inventory {
public:
	Inventory ();
	void Update (string item, int amount);
	void ListByName ();
	void ListByQuantity ();
private:
	vector<pair<string, int> > inventoryList;
	vector<pair<string, int> > inventoryQuantity;
};