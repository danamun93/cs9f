#include <vector>
#include <string>
#include "exercise2.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

using namespace std;
void InterpretCommands (istream&, Inventory*);
void InterpretUpdate (istream&, Inventory*);
void InterpretList (istream&, Inventory*);
void InterpretBatch (istream&, Inventory*);
void InterpretQuit (istream&);

Inventory::Inventory () {
 inventoryList.clear();
 inventoryQuantity.clear();
}

void Inventory::Update (string item, int amount) {
	bool wasInInven = false;
	for (int i = 0; i < inventoryList.size(); i++) {
		if (inventoryList[i].first == item) {
			inventoryList[i].second = amount;
			wasInInven = true;
		}
		if (inventoryQuantity[i].first == item) {
			inventoryQuantity[i].second = amount;
		}
	} 
	if (!wasInInven) {
		std::pair <std::string,int> p (item,amount);
		int count = inventoryList.size();
	  inventoryList.push_back(p);       
	  int loc = count;
	  while (0 < loc && p.first <= inventoryList[loc-1].first) {   
	    inventoryList[loc] = inventoryList[loc-1];
			loc--; 
		}
	  inventoryList[loc] = p;

	  count = inventoryQuantity.size();
	  inventoryQuantity.push_back(p);       
	  loc = count;
	  while (0 < loc && p.second <= inventoryQuantity[loc-1].second) {   
	    inventoryQuantity[loc] = inventoryQuantity[loc-1];
			loc--; 
		}
	  inventoryQuantity[loc] = p;
	}
}

void Inventory::ListByName () {
	for (int i = 0; i < inventoryList.size(); i++) {
		cout << inventoryList[i].first << " => " << inventoryList[i].second << endl;
	} 
}

void Inventory::ListByQuantity () {
	for (int i = 0; i < inventoryQuantity.size(); i++) {
		cout << inventoryQuantity[i].first << " => " << inventoryQuantity[i].second << endl;
	} 
}

void InterpretCommands (istream& cmds, Inventory* k) {
  string line, cmdWord;
  //____________________________ ;  // other definitions go here
  while ( getline(cmds, line) ) {   // get a line
    istringstream lineIn (line.c_str());
    if ( !(lineIn >> cmdWord) ) {  // get the line's first word
      cout << cmdWord << endl;
      cerr << "No words" << endl; // there weren't any words
    } else if (cmdWord=="update") {
      InterpretUpdate (lineIn, k);
    } else if (cmdWord=="list") {
      InterpretList (lineIn, k);
    } else if (cmdWord=="batch") {
      InterpretBatch (lineIn, k);
    } else if (cmdWord=="quit") {
      InterpretQuit (lineIn);
    } else {
      cerr << "Unrecognizable command word." << endl;
    }
  }
}

void InterpretUpdate (istream& is, Inventory* k) {
  string word;
  int update;
  if (is >> word >> update) {
    // string and integer values were successfully read
    k->Update(word, update);
  } else {
    // string and integer values were not successfully read
    cerr << "Unrecognizable command word." << endl;
  }
}

void InterpretList (istream& is, Inventory* k) {
  string word;
  if (is >> word) {
    if (word == "names") {
      k->ListByName();
    } else if (word == "quantities") {
      k->ListByQuantity();
    } else {
      cerr << "Unrecognizable command word." << endl;
    }
  } else {
    // string and integer values were not successfully read
    cerr << "Unrecognizable command word." << endl;
  }
}

void InterpretBatch (istream& is, Inventory* k) {
  //do the following: read, recognize, and process commands from the specified file, 
  //then (assuming that a quit command has not been read) resume reading commands from cin
  string filename;
  if (is >> filename) {
    //open the file
    string line;
    ifstream myfile (filename);
    if (myfile.is_open()) {
      while ( getline (myfile,line) ) {
        istringstream lineIn (line.c_str());
        InterpretCommands(lineIn, k);
      }
      myfile.close();
    }
    else {
      cout << "Unable to open file"; 
    }
  } else {
    // string and integer values were not successfully read
    cerr << "Unrecognizable command word." << endl;
  }
}

void InterpretQuit (istream& is) {
  exit(0);
}

int main () {
  Inventory k = Inventory();
  InterpretCommands (cin, &k);
  return 0;
}

/*int main () {
  Inventory k = Inventory();
  k.Update("E", 90);
  k.Update("E", 0);
  k.Update("F", 10);
  k.Update("AB", 1);
  k.Update("A", 2);
  k.Update("D", 3);
  k.Update("Coco", 4);
  k.ListByName();
  k.ListByQuantity();
  return 0;
}*/