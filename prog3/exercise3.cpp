#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

#include "exercise3.h"

using namespace std;
int size = 3;

Board::Board () {
	for (int i = 0; i<size; i++) {
	    vector<char> myvector;
	    for(int j = 0; j<size; j++) {
	    	char c = '.';
	        myvector.push_back(c);
	    }
	    board.push_back(myvector);
	}
}

void Board::SetCell (int player, int row, int col, char c) {
	if (player == 0) {
		board[row][col] = c;
	} else {
		board[(size-1)-row][col-(size-1)] = c;
	}
}

void Board::Print (int player) {
	if (player == 0) {
		for (int i = 0; i<size; i++) {
		    for(int j = 0; j<size; j++) {
		        cout << board[i][j] << " ";
		    }
		    cout << "\n";
		}
	} else {
		for (int i = size-1; i >= 0; i--) {
		    for(int j = size-1; j >= 0; j--) {
		        cout << board[i][j] << " ";
		    }
		    cout << "\n";
		}
	}
	
}


int main() {
	Board b = Board();
	b.SetCell(0,0,0,'x');
	b.SetCell(0,0,1,'o');
	b.SetCell(0,0,2,'x');
	b.SetCell(0,1,0,'o');
	b.SetCell(0,1,1,'x');
	b.SetCell(0,1,2,'.');
	b.SetCell(0,2,0,'o');
	b.SetCell(0,2,1,'x');
	b.SetCell(0,2,2,'o');

	//b.SetCell(0,0,2,'x');
	b.SetCell(0,0,2,'M');
	/*b.SetCell(0,0,0,'x');
	b.SetCell(0,0,1,'o');
	b.SetCell(0,0,2,'x');
	b.SetCell(0,1,0,'o');
	b.SetCell(0,1,1,'x');
	b.SetCell(0,1,2,'.');
	b.SetCell(0,2,0,'o');
	b.SetCell(0,2,1,'x');
	b.SetCell(0,2,2,'o');*/

	b.Print(0);
	b.Print(1);
}