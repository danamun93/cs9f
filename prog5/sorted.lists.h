#ifndef SORTEDLIST_H
#define SORTEDLIST_H

#include <iostream>
#include <cassert>

template <class NODETYPE> class SortedList;
template <class NODETYPE> class SortedListIterator;
template <class NODETYPE>

class ListNode {
	friend class SortedList<NODETYPE>;
	friend class SortedListIterator<NODETYPE>;
public:
	ListNode (const NODETYPE &);
	NODETYPE Info ();
private:
	NODETYPE myInfo;
	ListNode* myNext;
};

template <class NODETYPE>
ListNode<NODETYPE>::ListNode (const NODETYPE &value) {
	myInfo = value;
	myNext = 0;
}

template <class NODETYPE>
NODETYPE ListNode<NODETYPE>::Info () {
	return myInfo;
}

template <class NODETYPE>
class SortedList {
friend class SortedListIterator<NODETYPE>;
public:
	SortedList ();
	~SortedList ();
	SortedList (const SortedList <NODETYPE> &);
	void Insert (const NODETYPE &);
	bool IsEmpty ();
	SortedList<NODETYPE>& operator= (const SortedList <NODETYPE>&);
private:
	ListNode <NODETYPE>* myFirst;
};

template <class NODETYPE>
SortedList<NODETYPE>::SortedList() {	// constructor
	myFirst = 0;
}

template <class NODETYPE>
SortedList<NODETYPE>::~SortedList () {	// destructor
	if (!IsEmpty ()) {
		cerr << "*** in destructor, destroying: ";
		ListNode <NODETYPE>* current = myFirst;
		ListNode <NODETYPE>* temp;
		while (current != 0) {
			cerr << " " << current->myInfo;
			temp = current;
			current = current->myNext;
			delete temp;
		}
		cerr << endl;
	}
}

template <class NODETYPE>
SortedList<NODETYPE>::SortedList (const SortedList <NODETYPE> &list) {	// copy constructor
	// copy's argument list in a variable called newCurrent, but how do we access new current? 
	cerr << "*** in copy constructor" << endl;
	ListNode <NODETYPE>* listCurrent = list.myFirst;
	ListNode <NODETYPE>* newCurrent = 0;
	while (listCurrent != 0) {
		ListNode <NODETYPE> *temp = new ListNode <NODETYPE> (listCurrent->Info ());
		if (newCurrent == 0) {
			myFirst = temp;
			newCurrent = myFirst;
		} else {
			newCurrent->myNext = temp;
			newCurrent = temp;
		}
		listCurrent = listCurrent->myNext;
	}
}

template <class NODETYPE>
void SortedList<NODETYPE>::Insert (const NODETYPE &value) {	// Insert
	ListNode <NODETYPE> *toInsert = new ListNode <NODETYPE> (value);
	if (IsEmpty ()) {
		myFirst = toInsert;
	} else if (value < myFirst->Info ()) {
		toInsert->myNext = myFirst;
		myFirst = toInsert;
	} else {
		ListNode <NODETYPE> *temp = myFirst;
		for (temp = myFirst; 
			  temp->myNext != 0 && temp->myNext->Info () < value; 
			  temp = temp->myNext) {
		}
		toInsert->myNext = temp->myNext;
		temp->myNext = toInsert;
	}
}

template <class NODETYPE>
bool SortedList<NODETYPE>::IsEmpty (){	// IsEmpty
	return myFirst == 0;
}

template <class NODETYPE>
SortedList<NODETYPE>& SortedList<NODETYPE>::operator= (const SortedList <NODETYPE> &right) {
	//Your function should first delete all the ListNode objects in the variable being assigned to 
	//(the left hand side of the =). It should then construct a copy of the list on the right hand side of the = 
	//to assign to the variable on the left hand side.  
	if (this == &right) {
		return *this;
	} else {
		if (!IsEmpty ()) {
			ListNode <NODETYPE>* current = myFirst;
			ListNode <NODETYPE>* temp;
			while (current != 0) {
				temp = current;
				current = current->myNext;
				delete temp;
			}
		}
		ListNode <NODETYPE>* listCurrent = right.myFirst;
		ListNode <NODETYPE>* newCurrent = 0;
		while (listCurrent != 0) {
			ListNode <NODETYPE> *temp = new ListNode <NODETYPE> (listCurrent->Info ());
			if (newCurrent == 0) {
				myFirst = new ListNode <NODETYPE> (listCurrent->Info ());
				newCurrent = myFirst;
			} else {
				newCurrent->myNext = temp;
				newCurrent = temp;
			}
			listCurrent = listCurrent->myNext;
		}
	}
	return *this;
}

template <class NODETYPE>
class SortedListIterator {
public:
	SortedListIterator (SortedList<NODETYPE>);
	bool MoreRemain ();
	NODETYPE Next();
private:
	SortedList<NODETYPE> myList;
	//ListNode<NODETYPE> *myCurrNode;
};

template <class NODETYPE>
SortedListIterator<NODETYPE>::SortedListIterator(SortedList<NODETYPE> list) {	// constructor
	myList = list;
	//myCurrNode = myList.myFirst;
}

template <class NODETYPE>
bool SortedListIterator<NODETYPE>::MoreRemain() {
	if (myList.myFirst != 0) {
		return true;
	} else {
		return false;
	}
}

template <class NODETYPE>
NODETYPE SortedListIterator<NODETYPE>::Next() {
	NODETYPE temp = myList.myFirst->myInfo; 
	myList.myFirst = myList.myFirst->myNext;
	return temp;
}

#endif