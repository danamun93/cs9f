#include <vector>
#include <string>
#include <iostream>
using namespace std;

#include "positions.h"
#include "animals.h"
#include "park.h"

bool Mouse::Chase () {
	myPos.IncrementPosition(0, 1);
	cout << "mouse goes " << myPos << "\n";
	return 0;
}

bool Cat::Chase () {
	Position old = Position(myPos.myRadius, myPos.myAngleInRadians);
	// if the cat see's mouse and is not at the status
	if (myPos.Sees(myTarget->Pos()) and !myPos.IsAtStatue()) {
		myPos.IncrementPosition(1, 0);
		cout << "cat sees mouse " << myPos << "\n";
	// if the cat is at the statue or it didnt see the mouse, the move the cat around
	} else {
		float theta = 1.25 / myPos.myRadius;
		myPos.IncrementPosition(0, theta);
		cout << "cat dosent see mouse " << myPos << "\n";
	}

	if (myPos.IsAtStatue()) {
		if (myTarget->Pos().IsBetween(old, myPos)) {
			cout << "cat caught mouse " << "\n";
			return 1;
		}
	} 
	
	return 0;
}

bool Person::Chase () {
	Position old = Position(myPos.myRadius, myPos.myAngleInRadians);
	// if dosent see target it will move 2 meters around the statue
	if (!myPos.Sees(myTarget->Pos())) {
		float theta = 2 / myPos.myRadius;
		myPos.IncrementPosition(0, theta);
		cout << "person dosen't see target and moves " << myPos << "\n";
	} else {
		cout << "person sees target " << myPos << "\n";
	}
	return 0;
}

