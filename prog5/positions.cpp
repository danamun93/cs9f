#include <iostream>
#include <sstream> 
#include <cmath>
#include "positions.h"

using namespace std;

// radius = 1, angle = 0.
Position::Position () {
	myRadius = 1;
	myAngleInRadians = 0;
}	

// angle = 0.
Position::Position (float r) {
	myRadius = r;
	myAngleInRadians = 0;
}

Position::Position (float r, float thetaInRadians) {
	myRadius = r;
	myAngleInRadians = thetaInRadians;
}

// Reinitialize this position.
void Position::SetAbsolutePosition (float r, float thetaInRadians) {
	myRadius = r;
	myAngleInRadians = thetaInRadians;
}

// Change this position, incrementing the radius by rChange
// and incrementing the angle by thetaChange (expressed in radians).
// One of the two arguments must be 0.
// Negative radius values represent movement toward the statue. 
// Positive angular distance changes represent 
// counterclockwise motion; negative values are clockwise.
// thetaChange = 1.25 for cat, 1.0 for mouse
void Position::IncrementPosition (float rChange, float thetaChange) {
	//theta change is expressed in radians. 
	if (rChange != 0) {
		if (rChange == 1 and myRadius < 2 and myRadius > 1) {
			myRadius = 1;
		} else {
			myRadius -= rChange;
		}
	} 
	/*else if (anglechange != 0){
		float arc_radius = myRadius * myAngleInRadians;
		myAngleInRadians = fmod(anglechange / arc_radius, 6.28318530717958647688);
	} */
	myAngleInRadians = Normalize(myAngleInRadians + thetaChange);

}

// Compare two positions.
bool Position::operator== (Position coords) {
	return (myRadius == coords.myRadius) && (myAngleInRadians == coords.myAngleInRadians);
}

// Print this position.
void Position::Print ( ) {
	cout << this; 
}

/*string Position::tostring() const {
	ostringstream os;
	os.fill(’0’);
	os << "Radius" << myRadius << "Angle" << myAngleInRadians;
	return os.str();
}*/

ostream& operator<< (ostream &out, Position &pos) {
	std::ostringstream stringStream;
    stringStream << "radius: " << pos.myRadius << ", " << "angle in radians: " << pos.myAngleInRadians << "\n";  
	out << stringStream.str();
    return out;
}

// Return true if someone at this position can see someone or
// something at the argument position (i.e. the statue does
// not block one's view), and return false otherwise.
// The cat sees the mouse if
// (cat radius) * cos (cat angle - mouse angle)
// is at least 1.0.
// The argument position must be at the statue.
bool Position::Sees (Position pos) {
	return (myRadius * cos(myAngleInRadians - pos.myAngleInRadians)) >= 1.0;
}

// Return true if this position is at the base of the statue,
// i.e. its radius is 1, and return false otherwise.
bool Position::IsAtStatue ( ) {
	return myRadius == 1;
}

// Return true if this position is between the first argument
// position and the second.  Precondition: the counterclockwise
// difference between the first and second argument positions 
// is less than pi radians, and the radii of all the positions 
// are the same.
// 1) cos (B-A) > cos (C-A)
// 2) cos (C-B) > cos (C-A)
bool Position::IsBetween (Position old, Position current) {
	float arc_radius = old.myRadius * old.myAngleInRadians;
	float pos1A = -1.25 / arc_radius;
	float BminusA = cos(myAngleInRadians - pos1A);
	float CminusA = cos(current.myAngleInRadians - pos1A);
	float CminusB = cos(current.myAngleInRadians - myAngleInRadians);

	if (BminusA > CminusA and CminusB > CminusA) {
		return true;
	} else {
		return false;
	}
}

float Position::Normalize (float radians) {
	return fmod(radians, 6.28318530717958647688);
}