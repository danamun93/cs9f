#ifndef ANIMALS_H
#define ANIMALS_H
#include <vector>
#include <string>

#include "positions.h"
#include "park.h"

class Mouse : public Animal {
	// A Mouse object doesn't "chase" anything; it merely moves counterclockwise around the statue, 
	// one meter per call to Chase. Therefore, use the mouse's radius as the statue's radius.
public:
	Mouse(string name, Position startingPos) : Animal(name, startingPos) {}
	// Move the animal, and return true if it catches its target.
	virtual bool Chase ();
};

class Cat : public Animal {
	// A Cat object has a Mouse object as its target. If the cat sees its target, 
	// it moves one meter toward the statue; otherwise it circles 1.25 meters counterclockwise around the statue.
public:
	Cat(string name, Position startingPos): Animal(name, startingPos) {}
	// Move the animal, and return true if it catches its target.
	virtual bool Chase ();
};

class Person : public Animal {
	//A Person object is trying to photograph the situation. It doesn't try to capture anything. 
	// If it sees its target, it doesn't move; otherwise it circles 2 meters clockwise around the statue
public:
	Person(string name, Position startingPos): Animal(name, startingPos) {}
	// Move the animal, and return true if it catches its target.
	virtual bool Chase ();
};

#endif